# imgcomp.js

a simple library to compress and upload images, in-browser.

## install

```sh
npm i git+https://0xacab.org/hwm/imgcomp.js
```

## API

### `compressImage(file: Blob) -> Promise<Blob>`

pass in a file, and the lib will try to compress (only `jpg` or `png`) into a jpeg image.

## quirks

if you have `privacy.resistFingerprinting` enabled in Firefox, the output image will be blank. to fix, give the "HTML5 canvas image data" permission to the website.
