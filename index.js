const _ = type => (...args) => console[type]('[imgcomp]', ...args)
const log = _('log')
const error = _('error')

const _compressImage = file =>
  new Promise((resolve, reject) => {
    const img = new Image()
    img.src = URL.createObjectURL(file)
    img.onload = () => {
      const canvas = document.createElement('canvas')

      // limitar a 1920x1080
      const isLandscape = img.width > img.height
      if (
        isLandscape
          ? img.width > 1920 || img.height > 1080
          : img.width > 1080 || img.height > 1920
      ) {
        if (isLandscape) {
          ;[canvas.width, canvas.height] = [
            1920,
            (1920 * img.height) / img.width,
          ]
        } else {
          ;[canvas.width, canvas.height] = [
            (1080 * img.width) / img.height,
            1080,
          ]
        }
      } else {
        ;[canvas.width, canvas.height] = [img.width, img.height]
      }

      const ctx = canvas.getContext('2d')
      ctx.drawImage(img, 0, 0, canvas.width, canvas.height)

      if (canvas.toBlob) {
        log('using canvas.toBlob')
        canvas.toBlob(blob => resolve(blob), 'image/jpeg', 0.7)
      } else {
        log('using canvas.toDataURL')
        const url = canvas.toDataURL('image/jpeg', 0.7)
        resolve(fetch(url).then(res => res.blob()))
      }
    }
  })

async function compressImage(file) {
  if (
    // confirmamos que solo sea jpg/png para no romper la imagen, ej. si es un gif
    /^image\/(jpeg|png)$/i.test(file.type)
  ) {
    try {
      const blob = await _compressImage(file)
      return blob
    } catch (error) {
      error(`couldn't compress image`, error)
      // fallback a mandar sin comprimir
    }
  }

  // mandar sin comprimir
  return file
}

export { compressImage }
